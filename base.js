$(document).ready(function() {
    $('.burger svg').on('click', function() {
        $('.burger').toggleClass('active');
        $('.menu__mobile').toggleClass('active');
        $('body').toggleClass('overflow-none');
    })
})